Project Name:  SOT-23 SMD Breakout PCB
Initiation Date:  2021-05-15


This SOT-23 breakout PCB was created so that I could use some SOT-23 type transistors and MOSFETs on a breadboard.


Useful URLs:
------------

Hardware:
---------


PCB Versions:
-------------

Version 1.0:  JLCPCB, 1.6mm, 2 layer, white PCB
Production Date: 2021-04-30
PCB Details:

* initial version of the SOT-23 PCB.  


Code Information:
-----------------
N/A


Project Journal:
----------------
2021-04-30:  files uploaded to JLCPCB
2021-04-27:  PCB designed up in KiCAD


































