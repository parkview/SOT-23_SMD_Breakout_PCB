EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "SOT-23 Breakout"
Date "2021-04-27"
Rev "1.0"
Comp "Parkview Research Labs"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R_Small Rpdwn1
U 1 1 5EBE2709
P 5625 3055
F 0 "Rpdwn1" H 5660 2980 50  0000 L CNN
F 1 "10KR" H 5385 2965 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5625 3055 50  0001 C CNN
F 3 "~" H 5625 3055 50  0001 C CNN
	1    5625 3055
	-1   0    0    1   
$EndComp
$Comp
L Pauls_Symbol_Library:AO3401A Q1
U 1 1 608A3BDB
P 5390 3200
F 0 "Q1" H 5265 3115 50  0000 L CNN
F 1 "AO3401A" H 5195 3350 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 5590 3125 50  0001 L CIN
F 3 "http://www.aosmd.com/pdfs/datasheet/AO3401A.pdf" H 5390 3200 50  0001 L CNN
	1    5390 3200
	-1   0    0    1   
$EndComp
Wire Wire Line
	5590 3200 5625 3200
Wire Wire Line
	5625 3155 5625 3200
Connection ~ 5625 3200
Wire Wire Line
	5625 3200 5670 3200
Wire Wire Line
	5870 3200 5900 3200
$Comp
L Device:R_Small Rpup1
U 1 1 608AA5A0
P 5625 3350
F 0 "Rpup1" H 5675 3450 50  0000 L CNN
F 1 "10KR" H 5390 3455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5625 3350 50  0001 C CNN
F 3 "~" H 5625 3350 50  0001 C CNN
	1    5625 3350
	-1   0    0    1   
$EndComp
Wire Wire Line
	5625 3200 5625 3250
Wire Wire Line
	5945 3100 5920 3100
Wire Wire Line
	5920 3100 5920 2915
Wire Wire Line
	5920 2915 5625 2915
Wire Wire Line
	5290 2915 5290 3000
Wire Wire Line
	5625 2955 5625 2915
Connection ~ 5625 2915
Wire Wire Line
	5625 2915 5290 2915
Wire Wire Line
	5945 3300 5915 3300
Wire Wire Line
	5915 3300 5915 3490
Wire Wire Line
	5915 3490 5625 3490
Wire Wire Line
	5290 3490 5290 3400
Wire Wire Line
	5625 3450 5625 3490
Connection ~ 5625 3490
Wire Wire Line
	5625 3490 5290 3490
$Comp
L Device:R_Small Rg1
U 1 1 608A7A81
P 5770 3200
F 0 "Rg1" V 5855 3065 50  0000 L CNN
F 1 "150R" V 5700 3040 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5770 3200 50  0001 C CNN
F 3 "~" H 5770 3200 50  0001 C CNN
	1    5770 3200
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x03_Male J1
U 1 1 608A48C2
P 6145 3200
F 0 "J1" H 6255 3395 50  0000 R CNN
F 1 "Conn_01x03_Male" H 6340 3035 50  0001 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Horizontal" H 6145 3200 50  0001 C CNN
F 3 "~" H 6145 3200 50  0001 C CNN
	1    6145 3200
	-1   0    0    -1  
$EndComp
$Comp
L Device:D_Schottky_Small D1
U 1 1 608D8595
P 5130 3205
F 0 "D1" V 5035 3045 50  0000 L CNN
F 1 "Schottky_Small" V 5210 2605 50  0000 L CNN
F 2 "Diode_SMD:D_0805_2012Metric" V 5130 3205 50  0001 C CNN
F 3 "~" V 5130 3205 50  0001 C CNN
	1    5130 3205
	0    1    1    0   
$EndComp
Wire Wire Line
	5130 3105 5130 2915
Wire Wire Line
	5130 2915 5290 2915
Connection ~ 5290 2915
Wire Wire Line
	5130 3305 5130 3490
Wire Wire Line
	5130 3490 5290 3490
Connection ~ 5290 3490
$Comp
L Device:R_Small R1
U 1 1 608D9E5B
P 5765 3785
F 0 "R1" V 5835 3740 50  0000 L CNN
F 1 "2K" V 5695 3740 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5765 3785 50  0001 C CNN
F 3 "~" H 5765 3785 50  0001 C CNN
	1    5765 3785
	0    1    1    0   
$EndComp
$Comp
L Device:LED_Small D2
U 1 1 608DADF1
P 5625 3650
F 0 "D2" V 5570 3490 50  0000 L CNN
F 1 "Red" V 5690 3455 50  0000 L CNN
F 2 "LED_SMD:LED_0603_1608Metric" V 5625 3650 50  0001 C CNN
F 3 "~" V 5625 3650 50  0001 C CNN
	1    5625 3650
	0    1    1    0   
$EndComp
Wire Wire Line
	5625 3550 5625 3490
Wire Wire Line
	5625 3750 5625 3785
Wire Wire Line
	5625 3785 5665 3785
Wire Wire Line
	5865 3785 5935 3785
Wire Wire Line
	5935 3785 5935 3225
Wire Wire Line
	5935 3225 5900 3225
Wire Wire Line
	5900 3225 5900 3200
Connection ~ 5900 3200
Wire Wire Line
	5900 3200 5945 3200
Text Notes 6160 3235 0    50   ~ 0
Gate
Text Notes 6155 3135 0    50   ~ 0
Drain/Source
Text Notes 6160 3330 0    50   ~ 0
Source/Drain
Text Notes 5175 2825 0    50   ~ 0
* Fit pull-up resistor for P-MOS\nor pull-down for N-MOS\nQ1 MOSFETs
Text Notes 4585 4190 0    50   ~ 0
Notes:\n----\n* the AO3401 SOT-23 MOSFET is an example only\n* LED and Resistor are Optional\n* Schottky Diode is optional
$EndSCHEMATC
